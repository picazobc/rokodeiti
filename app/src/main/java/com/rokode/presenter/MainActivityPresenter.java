package com.rokode.presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;


import com.rokode.R;
import com.rokode.interfaces.MainActivityInterface;
import com.rokode.model.Encuesta;
import com.rokode.model.Imagenes;
import com.rokode.utils.SharedPreferences;
import com.rokode.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by PicazoBc
 */
public class MainActivityPresenter implements MainActivityInterface {
    private MainActivity mView;
    private SharedPreferences sharedPreferences;
    HttpURLConnection conn;
    Context context;
    private List<Encuesta> encuestaList = new ArrayList<>();
    private List<Imagenes> imagenesList = new ArrayList<>();



    public MainActivityPresenter(Context activity, MainActivity mView) {
        this.context = activity;
        this.mView = mView;
    }

    @Override
    public void getEncuesta(Context context) {
        if(isNetworkAvailable(context)){
            sharedPreferences = SharedPreferences.getInstance();
            new GetEncuesta().execute(context.getResources().getString(R.string.url_encuestas));
        }
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    class GetEncuesta extends AsyncTask<String,Integer,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url;
            System.out.println("Consumir: " + cadena);

            try {
                HttpURLConnection urlConn;

                url = new URL(cadena);
                urlConn = (HttpURLConnection) url.openConnection();
                urlConn.setDoInput(true);
                urlConn.setDoOutput(true);
                urlConn.setUseCaches(false);
                urlConn.setRequestProperty("Content-Type", "application/json");
                urlConn.setRequestProperty("connection", "close");

                urlConn.connect();

                JSONObject jsonParam = new JSONObject();


                OutputStream os = urlConn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(jsonParam.toString());
                writer.flush();
                writer.close();

                int respuesta = urlConn.getResponseCode();

                StringBuilder result = new StringBuilder();

                if (respuesta == HttpsURLConnection.HTTP_OK) {

                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result.append(line);
                    }

                    JSONObject respuestaJSON = new JSONObject(result.toString());
                    Log.d("LOGIN RESPONSE", respuestaJSON.toString());
                    JSONArray jsonArrayEncuestas = respuestaJSON.getJSONArray("questions");
                    JSONObject textLayout = respuestaJSON.getJSONObject("text");

                    final HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("welcomeTitle",textLayout.getString("welcomeTitle"));
                    hashMap.put("welcomeSubtitle",textLayout.getString("welcomeSubtitle"));
                    hashMap.put("button_text",textLayout.getString("start"));

                    sharedPreferences.setStringData("botton_text_next",textLayout.getString("next"));
                    sharedPreferences.setStringData("thanksTitle",textLayout.getString("thanksTitle"));
                    sharedPreferences.setStringData("thanksDescription",textLayout.getString("thanksDescription"));
                    sharedPreferences.setStringData("thanksSubtitle",textLayout.getString("thanksSubtitle"));
                    sharedPreferences.setStringData("endLink",textLayout.getString("endLink"));
                    sharedPreferences.setStringData("endLinkText",textLayout.getString("endLinkText"));


                    for(int i = 0; i<jsonArrayEncuestas.length();i++){
                        JSONObject jsonObject = jsonArrayEncuestas.getJSONObject(i);
                        JSONArray jsonArray = jsonObject.getJSONArray("ratingImgs");

                        Encuesta productos = new Encuesta(jsonObject.getString("idQuestion"), jsonObject.getString("type"),jsonObject.getString("text"));
                        encuestaList.add(productos);

                        for(int i1 = 0; i1<jsonArray.length();i1++){
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i1);
                            Imagenes imagenes = new Imagenes(jsonObject1.getString("url"),jsonObject1.getString("value"));
                            imagenesList.add(imagenes);
                        }

                    }

                    mView.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            mView.showStart(hashMap);
                            mView.setElements(encuestaList,imagenesList);
                        }
                    });
                } else {
                    Toast.makeText(mView.getApplicationContext(),"Intenta mas tarde",Toast.LENGTH_LONG).show();

                }
            } catch (java.net.SocketTimeoutException e) {
                System.out.println("SocketTimeoutException | SocketTimeoutException: " + e);
                Log.d("Responses", "Error timeout");
                return "";
            } catch (IOException e) {
                System.out.println("IOException | JSONException: " + e);
                Log.d("Responses", "Error");
                return "";
            } catch (JSONException e) {
                System.out.println("IOException | JSONException: " + e);

            } catch (Exception e) {
                Log.i("Testing", "Any other exception"+e.toString());
                return "";
            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

}

