package com.rokode.interfaces;


import com.rokode.model.Encuesta;
import com.rokode.model.Imagenes;

import java.util.HashMap;
import java.util.List;


/**
 * Created by PicazoBc
 */
public interface MainActivityViewInterface {

    void showStart(HashMap<String,String> hashMap);
    void setElements(List<Encuesta> encuesta, List<Imagenes> imagenes);


}
