package com.rokode.interfaces;


import android.content.Context;
import android.view.View;

/**
 * Created by PicazoBc
 */
public interface MainActivityInterface {

    void getEncuesta(Context context);

    boolean isNetworkAvailable(Context context);

}
