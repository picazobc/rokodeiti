package com.rokode.view;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.rokode.R;
import com.rokode.utils.SharedPreferences;


public class FinalDialogFragment extends DialogFragment {

    ImageView close_announcement;
    TextView tv_title,tv_subtitle,tv_link;
    SharedPreferences sharedPreferences;


    public static FinalDialogFragment newInstance() {
        FinalDialogFragment f = new FinalDialogFragment();

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_final_dialog, container, false);
        sharedPreferences = SharedPreferences.getInstance();


        tv_title = view.findViewById(R.id.tv_welcome);
        tv_subtitle = view.findViewById(R.id.tv_instructions);
        tv_link = view.findViewById(R.id.tv_link);


        tv_title.setText(sharedPreferences.getStringData("thanksTitle"));
        tv_subtitle.setText(sharedPreferences.getStringData("thanksSubtitle"));
        tv_link.setText(sharedPreferences.getStringData("endLinkText"));


        tv_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharedPreferences.getStringData("endLink")));
                startActivity(browserIntent);
            }
        });



        close_announcement = view.findViewById(R.id.close_announcement);
        close_announcement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        toolbar.setTitle("");


        return view;
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            dismiss();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}