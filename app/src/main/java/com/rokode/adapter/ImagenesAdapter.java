package com.rokode.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.rokode.R;
import com.rokode.model.Imagenes;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ImagenesAdapter extends BaseAdapter {

    private Context context;
    private List<Imagenes> items;

    public ImagenesAdapter(Context context, List<Imagenes> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View rowView = view;

        if (view == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_images, viewGroup, false);
        }

        ImageView image = rowView.findViewById(R.id.image_view);

        Imagenes item = this.items.get(position);

        String urlImage = item.getUrl();

        Picasso.with(context) // Context
                .load(urlImage) // URL or file
                .into(image);

        return rowView;
    }
}