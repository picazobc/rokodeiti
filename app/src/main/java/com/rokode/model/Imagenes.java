package com.rokode.model;


import java.io.Serializable;

public class Imagenes implements Serializable {
    private String url, value;



    public Imagenes() {
    }

    public Imagenes(String url, String value) {
        this.url = url;
        this.value = value;
    }

    public String getUrl() {
        return url;
    }

    public String getValue() {
        return value;
    }

}