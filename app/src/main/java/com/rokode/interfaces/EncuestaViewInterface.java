package com.rokode.interfaces;


import com.rokode.model.Imagenes;

import java.util.List;

/**
 * Created by PicazoBc
 */
public interface EncuestaViewInterface {

    void setNotifyDataChange(List<Imagenes> imagenesList);


}
