package com.rokode.presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.rokode.R;
import com.rokode.interfaces.MainActivityInterface;
import com.rokode.model.Encuesta;
import com.rokode.utils.SharedPreferences;
import com.rokode.view.EncuestaActivity;
import com.rokode.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by PicazoBc
 */
public class EncuestaPresenter {
    private EncuestaActivity mView;
    Context context;


    public EncuestaPresenter(Context activity, EncuestaActivity mView) {
        this.context = activity;
        this.mView = mView;
    }

}

