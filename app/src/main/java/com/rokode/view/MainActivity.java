package com.rokode.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rokode.R;
import com.rokode.interfaces.MainActivityViewInterface;
import com.rokode.model.Encuesta;
import com.rokode.model.Imagenes;
import com.rokode.presenter.MainActivityPresenter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityViewInterface {


    private MainActivityPresenter mPresenter;
    RelativeLayout relative_main;
    public static ProgressBar progressBar;
    TextView tv_title, tv_body;
    Button startButton;
    public static Context appContext;
    List<Encuesta> encuestaList = new ArrayList<>();
    List<Imagenes> imagenesList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appContext = getApplicationContext();

        mPresenter = new MainActivityPresenter(this, this);


        tv_title = findViewById(R.id.tv_welcome);
        tv_body = findViewById(R.id.tv_instructions);

        relative_main = findViewById(R.id.relative_main);

        startButton = findViewById(R.id.button_init);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,EncuestaActivity.class);
                intent.putExtra("listImages", (Serializable) imagenesList);
                intent.putExtra("listEncuesta", (Serializable) encuestaList);
                startActivity(intent);

            }
        });

        progressBar = findViewById(R.id.progressBar);
        mPresenter.getEncuesta(getApplicationContext());


    }

    @Override
    public void showStart(HashMap<String,String> hashMap){
        tv_title.setText(hashMap.get("welcomeTitle"));
        tv_body.setText(hashMap.get("welcomeSubtitle"));
        startButton.setText(hashMap.get("button_text"));

        relative_main.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setElements(List<Encuesta> encuesta, List<Imagenes> imagenes) {
        encuestaList = encuesta;
        imagenesList = imagenes;
    }


}
