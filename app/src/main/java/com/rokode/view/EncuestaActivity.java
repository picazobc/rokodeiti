package com.rokode.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.rokode.R;
import com.rokode.adapter.ImagenesAdapter;
import com.rokode.interfaces.EncuestaViewInterface;
import com.rokode.model.Encuesta;
import com.rokode.model.Imagenes;
import com.rokode.presenter.EncuestaPresenter;
import com.rokode.utils.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class EncuestaActivity extends AppCompatActivity implements EncuestaViewInterface, AdapterView.OnItemClickListener {


    private EncuestaPresenter mPresenter;
    TextView tv_title;
    Button startButton;
    public static Context appContext;

    private GridView gridView;
    private ImagenesAdapter adaptador;
    private List<Imagenes> imagenes = new ArrayList<>();
    private List<Imagenes> imagenesTemp = new ArrayList<>();


    private List<Encuesta> encuestas = new ArrayList<>();
    private List<Encuesta> encuestaTemp = new ArrayList<>();

    SharedPreferences sharedPreferences;


    private int elementList=0;
    private Boolean isStart = true;
    private Boolean isEnd = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_encuesta);
        appContext = getApplicationContext();

        sharedPreferences = SharedPreferences.getInstance();
        mPresenter = new EncuestaPresenter(this, this);

        Intent intent = getIntent();
        imagenesTemp = (List<Imagenes>) intent.getSerializableExtra("listImages");
        encuestaTemp = (List<Encuesta>) intent.getSerializableExtra("listEncuesta");

        tv_title = findViewById(R.id.tv_title);

        if(isStart){
            isStart = false;

            for(int i=0;i<=3;i++){
                imagenes.add(imagenesTemp.get(i));
            }

            tv_title.setText(encuestaTemp.get(0).getText());

        }

        gridView = findViewById(R.id.gridView);
        adaptador = new ImagenesAdapter(this,imagenes);
        adaptador.notifyDataSetChanged();

        gridView.setAdapter(adaptador);
        gridView.setOnItemClickListener(this);


        startButton = findViewById(R.id.button_init);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(elementList != encuestaTemp.size()-1){
                    elementList++;
                    startButton.setText("");
                    reloadData(elementList);
                }else{
                    showDialogFinal();
                }

            }
        });



    }

    @Override
    public void setNotifyDataChange(List<Imagenes> imagenesList) {
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        startButton.setText(sharedPreferences.getStringData("botton_text_next"));
        Toast.makeText(getApplicationContext(),"Genial, continua",Toast.LENGTH_LONG).show();
    }


    private void reloadData(int elementList){
        imagenes.clear();
        for(int i= (elementList*encuestaTemp.size()) ;i<=  ((elementList+1)*encuestaTemp.size())-1  ;i++){

            imagenes.add(imagenesTemp.get(i));
        }
        adaptador.notifyDataSetChanged();
        tv_title.setText(encuestaTemp.get(elementList).getText());

    }

    private void showDialogFinal() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FinalDialogFragment newFragment = FinalDialogFragment.newInstance();

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment)
                .addToBackStack(null).commit();
    }

}
