package com.rokode.model;

import java.io.Serializable;

public class Encuesta implements Serializable {
    private String id, type, text;



    public Encuesta() {
    }

    public Encuesta(String id, String type, String text) {
        this.id = id;
        this.type = type;
        this.text = text;

    }


    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }
}